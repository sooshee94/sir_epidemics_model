# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 23:11:04 2018

@author: dandrona
"""

import random
import matplotlib.pyplot as plot
rand_generator = random.Random()
S_series = []
I_series = []
R_series = []
S = rand_generator.randint(0,1000)
I = rand_generator.randint(0,1000)
R = rand_generator.randint(0,1000)

N = S + I + R

#infection_time = 2.01
#recovery_time = 2

beta = 0.007
gamma = 0.0007
im = 0.00001


def tick(S, I, R ):
    global N
    global gamma
    global beta
    global im
    infected = (beta * I * S)/N
    recovered = gamma * I
    immunity_loss = im * R
    s_new = S - infected + immunity_loss
    i_new = I + infected - recovered
    r_new = R + recovered - immunity_loss
    return s_new, i_new, r_new

def is_within_bounds(x):
    global N
    if(x >= 0 and x <=N):
        return True
    return False

def plot_results(S_series,I_series,R_series):
    global N
    plot.plot(S_series, label = "Susceptible", color='green')
    plot.plot(I_series, label = "Infected", color='red')
    plot.plot(R_series, label = "Recovered")
    plot.legend()
    plot.ylim((0,N))
    plot.show()

def run_loop(S,I,R):
    S, I, R = tick(S,I,R)
    S_series.append(S)
    I_series.append(I)
    R_series.append(R)
    return S, I, R
def run_simulation(limit):
    global S, I, R
    if(limit == -1):
        while(S is not 0 and R is not N):
            S,I,R = run_loop(S, I, R)
    else:
        for i in range(limit):
            S, I, R = run_loop(S, I, R)
            if(not is_within_bounds(S) or not is_within_bounds(I) or not is_within_bounds(R)):
                break
    plot_results(S_series,I_series, R_series)
    
run_simulation(2500)